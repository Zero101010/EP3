class CreateProdutos < ActiveRecord::Migration
  def change
    create_table :produtos do |t|
      t.string :titulo
      t.text :descricao
      t.string :imagem_url
      t.float :valor
      t.string :categoria
      t.string :subcategoria

      t.timestamps null: false
    end
  end
end
